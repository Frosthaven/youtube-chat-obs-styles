/* CREDITS ********************************************************************/
/**
* @file Streamlabs chat custom JS. The latest version can always be found at
* the following url: https:   //bitbucket.org/Frosthaven/youtube-chat-obs-styles/
* @author Frosthaven @thefrosthaven
* @description - allows use of url parameters in widget
*/

/* CODE ***********************************************************************/
var css = document.createElement('style');

function getUrl() {
  var url = (window.location != window.parent.location)
          ? document.referrer
          :         document.location.href;
  return new URL(url);
}

function initCSS(label, fn) {
  css.type='text/css';
  css.setAttribute(label,'');
  css.innerHTML = '';

  fn(css);

  document.getElementsByTagName('head')[0].appendChild(css);
}

function addCSS(line) {
  css.innerHTML += `\n${line}`
}

function isOutOfBounds(elem) {

	// Get element's bounding
	var bounding = elem.getBoundingClientRect();

	// Check if it's out of the viewport on top or bottom
	var out = {};
	out.top = bounding.top < 0;
	out.bottom = bounding.bottom > (window.innerHeight || document.documentElement.clientHeight);
	out.any = out.top || out.bottom
	out.all = out.top && out.bottom

	return out;
}

function revealFullyVisible() {
  var elements = document.querySelectorAll('#log div:not([visible=false])');
  
  elements.forEach((el) => {
    if (el && isOutOfBounds(el).any) {
      el.setAttribute("visible","false");
    } else {
      el.setAttribute("visible","true");
    }
  });
}

function addBodyClass(name) {
  if (!document.body.classList.contains(name)) {
    document.body.className += ` ${name}`;
  }
}

function configureFromUrl(label,fn) {
  var theValue = getUrl().searchParams.get(label);
  if (theValue) {
    fn(theValue);
  }
}

function initStyles() {
  initCSS('injected', (css) => {
    // standalone styles
    configureFromUrl('style', (val) => {
      addBodyClass(val);
    });
  
    // autohide
    configureFromUrl('autohide', (val) => {
      if (val === 'true') {
        addBodyClass('autohide');
      }
    });
  
    // hide badges
    configureFromUrl('hidebadges', (val) => {
      if (val === 'true') {
        addBodyClass('hidebadges');
      }
    });
  
    // 3d transforms
    configureFromUrl('rotate', (val) => {
      val = val || "0";
      if (val !== "0") {
        addBodyClass('rotate');
        addCSS(`#log {  transform: perspective(1000px) rotateY(${val}deg) !important; padding:10%; }`)
      }
    });
  
    // username color
    configureFromUrl('namecolor', (val) => {
      val = val.replace('0x','#');
      addBodyClass('namecolor');
      addCSS(`.namecolor .name {color:${val} !important; }`)
    });
  
    // text color
    configureFromUrl('textcolor', (val) => {
      val = val.replace('0x','#');
      addBodyClass('textcolor');
      addCSS(`.textcolor .message {color:${val} !important; }`)
    });
  
    // bg color
    configureFromUrl('bgcolor', (val) => {
      val = val.replace('0x','#');
      addBodyClass('bgcolor');
      addCSS(`.bgcolor div[data-from] {background-color:${val} !important; }`)
    });
  
    // border color
    configureFromUrl('bordercolor', (val) => {
      val = val.replace('0x','#');
      addBodyClass('bordercolor');
      addCSS(`.bordercolor div[data-from] {border:1px solid ${val} !important; }`);
    });
  
    // border image
    configureFromUrl('bordergfx', (val) => {
      var params     = val.split(',');
      var addr       = params[0];
      var width      = params[1];
      var vertical   = params[2];
      var horizontal = params[3];
      var fill       = params[4];
  
      if (params && addr && width && vertical && horizontal && fill) {
        addBodyClass('bordergfx');
        addCSS(`.bordergfx div[data-from] { border: ${width}px solid transparent !important; -webkit-border-image: url(${addr}) ${vertical} ${horizontal} ${fill} !important; background:none !important; padding: 0px !important;}`);
      }
    });
    
    // hide partial
    configureFromUrl('hidepartial', (val) => {
      if (val === 'true') {
        addCSS(`.autohide div[data-from]:not([visible=true]), div[data-from]:not([visible=true]) { opacity:0; animation:none !important; }`);
  
        var observer = new MutationObserver(function(mutations) {
          // var element = mutations[0].addedNodes[0];
          // a new message has been added to the page
          revealFullyVisible();
        });
        observer.observe(document.getElementById('log'), {childList: true});
      }
    });
  });
}

// load style configurations
initStyles();
console.log('Chat Engine Loaded');